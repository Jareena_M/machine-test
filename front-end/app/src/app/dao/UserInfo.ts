import {UserContact} from "./UserContact";

export class UserInfo{

    public  id : bigint;
    public firstName : string;
    public lastName : string;
    public emailId : string;
    public userContact  = new UserContact;
    
}