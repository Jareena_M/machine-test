import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserInfo } from "../dao/UserInfo";

@Injectable({
  providedIn: 'root'
})


export class CrudService {
  
    constructor(private http: HttpClient) { }

    async addEntry(entry : UserInfo){
        return await this.http.post("http://localhost:8080/addorupdate", entry).toPromise();
    }

    async retrieveEntry(){
        return await this.http.get("http://localhost:8080/retrieve").toPromise();
    }

    async retrieveById(id : bigint){
        return await this.http.get("http://localhost:8080/retrievebyid/"+id).toPromise();
    }

    async deleteEntryById(id : bigint){
        return await this.http.delete("http://localhost:8080/deletebyid/"+id , {responseType: 'text'}).toPromise();
    }
}