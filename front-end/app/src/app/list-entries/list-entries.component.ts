import { Component, OnInit } from '@angular/core';
import { UserInfo } from "../dao/UserInfo";
import { CrudService } from "../services/CrudService";

@Component({
  selector: 'app-list-entries',
  templateUrl: './list-entries.component.html',
  styleUrls: ['./list-entries.component.css']
})
export class ListEntriesComponent implements OnInit {

  public userInfo : UserInfo = new UserInfo;
  public crudService: CrudService;
  public entries : UserInfo[] = [];
  public deleteMsg : any = null;

  constructor(private crudServiceImpl: CrudService) {
    this.crudService = crudServiceImpl;
  }

  ngOnInit(): void {
    this.retrieveData();
  }

  async retrieveData(){    
    var httpdata = await this.crudService.retrieveEntry();
    this.entries = <UserInfo[]> httpdata;
    console.log(this.entries);
  }

  async deleteEntryById( id : bigint){    
    await this.crudService.deleteEntryById(id).then( res=> this.deleteMsg = res );    
    this.retrieveData();     
  }

  closeDeleteMsg(){
    this.deleteMsg=null;
  }

}
