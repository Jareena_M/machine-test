import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddEntryComponent } from './add-entry/add-entry.component';
import { ListEntriesComponent } from './list-entries/list-entries.component';


const routes: Routes = [
  
  { path: 'add', component: AddEntryComponent },
  { path: 'add/:id', component: AddEntryComponent},
  { path: 'list', component: ListEntriesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
