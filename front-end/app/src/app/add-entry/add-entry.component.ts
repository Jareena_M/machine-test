import { Component, OnInit } from '@angular/core';
import { UserInfo } from "../dao/UserInfo";
import { CrudService } from "../services/CrudService";
import { ActivatedRoute } from "@angular/router"

@Component({
  selector: 'app-add-entry',
  templateUrl: './add-entry.component.html',
  styleUrls: ['./add-entry.component.css']
})

export class AddEntryComponent implements OnInit {

  public entry : UserInfo = new UserInfo;
  public crudService: CrudService;

  public successMsg : string ;
  public validationErrors : Map<string , any>;

  private id : bigint = null;
  private sub: any;

  public toggle = {
      firstName : false,
      lastName : false,
      emailId : false,
      userContact : {
      phoneNumber : false,
      alternateNumber : false
      }
  }

  constructor(private crudServiceImpl: CrudService , private route: ActivatedRoute) {
    this.crudService = crudServiceImpl;
   }

  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; 
      if(this.id!=null){
      this.retrieveById(this.id)
      }
      
   });
  }

  async retrieveById(id : bigint){
    await this.crudService.retrieveById(this.id).then( res=> this.entry = <UserInfo>res );
  }

  toggleShow( value : string){
    this.toggle[value] = false;
  }

  closeSuccessMsg(){
    this.successMsg = null;
  }
  
  async onSubmit(){
    await this.crudService.addEntry(this.entry)
    .then(res =>  {
      this.successMsg = "Successfully Added";
    })
    .catch(e =>{
      this.validationErrors = e.error.validationErrors;   
      if(this.validationErrors != undefined){        
        for (let [key , {type} ] of Object.entries(this.validationErrors)) {    
          var child = key.split(".");      

          if(child.length > 1 ){
            this.toggle[child[0]][child[1]] = true;
          }
          else{
            this.toggle[key] = true;
          }
        }
      }
    });        
  }

}
