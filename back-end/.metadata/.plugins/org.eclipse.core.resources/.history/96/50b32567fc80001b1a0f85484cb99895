package com.phonebook.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.phonebook.dao.Entry;
import com.phonebook.data.UserInfo;
import com.phonebook.service.CrudService;

@RestController("/")
public class CrudControllers {

	@Autowired
	CrudService crudService;
	
	/**
	 * Controller mapped to perform addition of an entry
	 * @param entry
	 * @return 
	 */
	
	@PostMapping("add")
	@CrossOrigin(origins = "http://localhost:4200")
    @ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Object> addEntry(@Valid @RequestBody Entry entry){
		Entry saveEntry = crudService.add(entry);
		 
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
		        .buildAndExpand(saveEntry.USER_INFO.getId()).toUri();

		    return ResponseEntity.created(location).build();
	}
	
	/**
	 * Controller to retrieve all phone book entries
	 * @return
	 */
	
	@GetMapping("retrieve")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<UserInfo>  retrieveEntries(){
		return crudService.retrieveAllEntries();
	}

}
