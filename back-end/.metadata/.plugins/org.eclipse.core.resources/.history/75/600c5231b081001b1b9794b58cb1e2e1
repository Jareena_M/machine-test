package com.phonebook.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.phonebook.data.UserInfo;
import com.phonebook.service.CrudService;

@RestController("/")
public class CrudControllers {

	@Autowired
	CrudService crudService;
	
	/**
	 * Controller mapped to perform addition of an entry
	 * @param entry
	 * @return 
	 */
	
	@PostMapping("addorupdate")
	@CrossOrigin(origins = "http://localhost:4200")
	public UserInfo addEntry(@Valid @RequestBody UserInfo entry){
		UserInfo saveEntry = crudService.add(entry);

		    return saveEntry;
	}
	
	/**
	 * Controller to retrieve all phone book entries
	 * @return
	 */
	
	@GetMapping("retrieve")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<UserInfo>  retrieveEntries(){
		return crudService.retrieveAllEntries();
	}
	
	@GetMapping("retrievebyid/{id}")
	@CrossOrigin(origins = "http://localhost:4200")
	public Optional<UserInfo>  retrieveById(@PathVariable Long id){
		return crudService.retrieveById(id);
	}
	
	@DeleteMapping("deletebyid/{id}")
	@CrossOrigin(origins = "http://localhost:4200")
	public String  deleteById(@PathVariable Long id){
		return crudService.deleteById(id);
	}

}
