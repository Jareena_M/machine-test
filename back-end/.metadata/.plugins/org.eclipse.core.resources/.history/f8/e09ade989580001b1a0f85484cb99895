package com.phonebook.data;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.phonebook.constant.Constants;


@Entity
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY ,
				getterVisibility = JsonAutoDetect.Visibility.NONE,
				setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class USER_INFO implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Column
	private String firstName;
	
	
	@NotNull(message = Constants.NOT_EMPTY)	
	private String lastName;
	
	
	@NotNull(message = Constants.NOT_EMPTY)
	@JsonFormat(shape = JsonFormat.Shape.STRING,
				pattern = "/[a-z0-9\\._%+!$&*=^|~#%{}/\\-]+@([a-z0-9\\-]+\\.){1,}([a-z]{2,22})/")	
	private String emailId;
	
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_info", referencedColumnName = "id")
	@Valid
    private USER_CONTACT USER_CONTACT;
	
	
	public USER_INFO() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}


	@NotNull(message = Constants.NOT_EMPTY)	
    @Size(min = 1, max = 30, message = "error.title.size")
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}	

	public USER_CONTACT getUSER_CONTACT() {
		return USER_CONTACT;
	}

	public void setUSER_CONTACT(USER_CONTACT uSER_CONTACT) {
		USER_CONTACT = uSER_CONTACT;
	}
}
