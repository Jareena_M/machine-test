package com.phonebook.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.phonebook.constant.Constants;

@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
@Entity
@Table(name = "USER_CONTACT")
public class UserContact {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	
	@NotNull(message = Constants.NOT_EMPTY)
	@JsonFormat(shape = JsonFormat.Shape.NUMBER, pattern = "[0-9]{10}")	
	private int phoneNumber;
	
	
	@NotNull(message = Constants.NOT_EMPTY)
	@JsonFormat(shape = JsonFormat.Shape.NUMBER, pattern = "[0-9]{10}" )	
	private int alternateNumber;
	

	
	@OneToOne(mappedBy = "userContact")
    private UserInfo userInfo;	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getAlternateNumber() {
		return alternateNumber;
	}

	public void setAlternateNumber(int alternateNumber) {
		this.alternateNumber = alternateNumber;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
		
}
