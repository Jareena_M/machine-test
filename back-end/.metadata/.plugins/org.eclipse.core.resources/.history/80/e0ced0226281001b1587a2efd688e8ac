package com.phonebook.data;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.phonebook.constant.Constants;


@Entity
@Table(name = "USER_INFO")

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY ,
				getterVisibility = JsonAutoDetect.Visibility.NONE,
				setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class UserInfo implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	

	@NotNull(message = Constants.NOT_EMPTY)	
	@NotEmpty(message = "Please provide a name")
    @Size(min = 1, max = 30, message = "Size should be greater than 1 and less than 30 characters")
	private String firstName;
	
	
	
	@NotNull(message = Constants.NOT_EMPTY)	
	private String lastName;
	
	
	
	@NotNull(message = Constants.NOT_EMPTY)
	@JsonFormat(shape = JsonFormat.Shape.STRING,
				pattern = "/[a-z0-9\\._%+!$&*=^|~#%{}/\\-]+@([a-z0-9\\-]+\\.){1,}([a-z]{2,22})/")	
	private String emailId;
	
	
	
	@Valid
	@NotNull
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contact_id", referencedColumnName = "id")	
    private UserContact userContact;
	
	
	public UserInfo() {
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}



	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}



	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}



	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}



	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}



	/**
	 * @return the userContact
	 */
	public UserContact getUserContact() {
		return userContact;
	}
	

	/**
	 * @param userContact the userContact to set
	 */
	public void setUserContact(UserContact userContact) {
		this.userContact = userContact;
	}	

	
}
