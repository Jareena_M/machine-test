package com.phonebook.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.phonebook.data.UserInfo;

public interface UserInfoRepo extends JpaRepository<UserInfo , Long>{

}
