package com.phonebook.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.phonebook.constant.Constants;

@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
@Entity
@Table(name = "USER_CONTACT")
public class UserContact {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	
	@NotNull(message = Constants.NOT_EMPTY)	
	@Length(min=10 , max =10, message =  Constants.ENTER_VALID_PHONE_NUMBER)
	private String phoneNumber;
	
	
	@NotNull(message = Constants.NOT_EMPTY)
	@Pattern(regexp = "[0-9]{10}" , message = Constants.ENTER_VALID_ALTERNATE_NUMBER)	
	private String alternateNumber;
	

	@JsonIgnore
	@OneToOne(mappedBy = "userContact")
	
	
    private UserInfo userInfo;	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAlternateNumber() {
		return alternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		this.alternateNumber = alternateNumber;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
		
}
