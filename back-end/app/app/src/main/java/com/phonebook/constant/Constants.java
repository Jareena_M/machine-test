package com.phonebook.constant;

public class Constants {

	public static final String NOT_EMPTY = "Field cannot be empty";
	public static final String ENTER_FIRST_NAME = "Enter first name"; 
	public static final String ENTER_LAST_NAME = "Enter last name";
	public static final String ENTER_VALID_MAIL_ID = "Please enter valid E-mail Id";
	public static final String ENTER_VALID_PHONE_NUMBER = "Please enter valid phone number";
	public static final String ENTER_VALID_ALTERNATE_NUMBER = "Please enter valid alternate phone number";
	public static final String SIZE_BETWEEN_3_30 = "Character length should be greater than 3 and less than 30";
	
	public Constants() {
		// TODO Auto-generated constructor stub
	}

}
