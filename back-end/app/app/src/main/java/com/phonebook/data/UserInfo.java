package com.phonebook.data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.phonebook.constant.Constants;


@Entity
@Table(name = "USER_INFO")

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY ,
				getterVisibility = JsonAutoDetect.Visibility.NONE,
				setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")


public class UserInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	

	@NotNull(message = Constants.NOT_EMPTY)	
	@NotEmpty(message = Constants.ENTER_FIRST_NAME)
    @Size(min = 3, max = 30, message = Constants.SIZE_BETWEEN_3_30)
	private String firstName;
	
	
	
	@NotNull(message = Constants.NOT_EMPTY)	
	@NotEmpty(message = Constants.ENTER_LAST_NAME)
    @Size(min = 1, max = 30, message = Constants.SIZE_BETWEEN_3_30 )
	private String lastName;
	
	
	
	@NotNull( message = Constants.NOT_EMPTY)
	@NotEmpty( message = Constants.NOT_EMPTY)	
	@Pattern(regexp = "^[a-zA-Z0-9+_.-]{3,}@[a-zA-Z0-9.-]+.[a-zA-Z0-9]+$" , 
		message= Constants.ENTER_VALID_MAIL_ID + " " +Constants.SIZE_BETWEEN_3_30)
	private String emailId;
	
	
	
	@Valid
	@NotNull( message = Constants.ENTER_VALID_PHONE_NUMBER )
	@OneToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "contact_id", referencedColumnName = "id")	
    private UserContact userContact;
	
	
	public UserInfo() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public UserContact getUserContact() {
		return userContact;
	}
	
	public void setUserContact(UserContact userContact) {
		this.userContact = userContact;
	}	

	
}
